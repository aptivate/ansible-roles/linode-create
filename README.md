[![pipeline status](https://git.coop/aptivate/ansible-roles/linode-create/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/linode-create/commits/master)

# linode-create

A role for creating new Linode machines.

Aptivate developer SSH accounts will automatically be created via the [ssh-user-mgmt] role.

[ssh-user-mgmt]: https://git.coop/aptivate/ansible-roles/ssh-user-mgmt

# Requirements

  * The `linode-python` python package. This must be installed in the
    environment in which the script runs. If testing with molecule, you do not
    need to install it. Otherwise, if you are calling this role from a
    playbook which runs in a virtual environment, and want the role to execute
    in the same virtual environment, you can set the following in your
    inventory: `localhost ansible_connection=local ansible_python_interpreter="{{ ansible_playbook_python }}"`.

  * The `LINODE_ACCESS_TOKEN` environment variable exposed. This is a variable that
    uses the version 4 of the API. A `LINODE_API_TOKEN` from API version 3 will
    not work.

  * The `sshpass` package.

# Role Variables

## Mandatory

  * `linode_label`: the Linode machine name. 32 characters maximum.

## With Defaults

  * `linode_type`: the Linode type.
    * Default is `g6-nanode-1` for the $5 Linode.

  * `linode_region`: the Linode region.
    * Default is `eu-west`.

  * `linode_distribution`: the Linode distribution.
    * Default is `linode/centos7`.

# Role Output

  * `linode_creation_details`: The Linode details stored in a variable.

# Dependencies

* https://git.coop/aptivate/ansible-roles/ssh-user-mgmt

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: linode-create
       linode_label: my-new-shiny-linode
```

# Testing

We're using Docker for our testing here.

```
$ pipenv install --dev
$ pipenv run molecule test
```

# License

* https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

* https://aptivate.org/
* https://git.coop/aptivate
